steps for install pip3 and virtualenv
1. sudo apt-get install python3-pip
2. pip3 install virtualenv 

step for the start the script for import the bulk users
1. download or clone the code from the bitbucket 
2. open that project in pycharm or any other IDE

3. run command virtualenv -p python3 enve in terminal
   if virtualenv is not installed then follow steps for install pip3 and virtualenv
4. after creating virtualenv activate that env
   command--> . enve/bin/activate
5. after that run command cd /<your project name or folder name>
6. run command pip install -r re.txt
7. after successfully installed dependencies run python server
   command-- python3 manage.py runserver 0.0.0.0:8000
8. after that open the view.py file and change the path of the images 
9. after changing the images path open postmen and set the parameter for the api
10. change the method "GET" to "POST"
11. after that set the url for the api ex...http://<ipaddress of the server>:8000/bulkImport_xlsx/
12. choose body and in that body select raw and in that line the last one is text in that choose JSON(application/json)
13. in body section paste
    {
	    "fileURL": "<path of the csv file>"
    }
    in this set the path of the csv file 
14. click on send button

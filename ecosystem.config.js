module.exports = {
  apps : [{
    name: 'userUpload',
    script: 'manage.py',
    args: 'runserver 0.0.0.0:8002',
    autorestart: true,
    watch: false,
    max_memory_restart: '1G',
    interpreter:'/opt/pythonApi/venve/bin/python3'
  }]
};

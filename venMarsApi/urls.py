from django.conf.urls import url, include
from django.contrib import admin
from django.contrib.auth import views as auth_views


from venMarsApi import views


app_name = "venMarsApi"

urlpatterns = [
    url(r'^bulkImport_xlsx/$', views.bulkImport_xlsx.as_view(), name='bulkImport_xlsx'),
]
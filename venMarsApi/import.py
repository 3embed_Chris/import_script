#from django.shortcuts import render
#from django.http import HttpResponse, JsonResponse
import pymongo
from pymongo import MongoClient, CursorType
from bson.objectid import ObjectId
import pandas as pd
from bson.objectid import ObjectId
from elasticsearch import Elasticsearch
#from rest_framework.views import APIView
import json
import requests
import datetime
import cloudinary
import cloudinary.uploader
import cloudinary.api
import random
import string
import glob

cloudinary.config(
    cloud_name="pipelineai",
    api_key="443435715668775",
    api_secret="l2iq6In_PajpDN6mAyNvjLDPcvU"
)

# Create your views here.
es = Elasticsearch(['http://elastic:c24j4svKBM8VtL8FSEM8@localhost:9200/'])
client = MongoClient('mongodb://chris:jHzgur6jW2nxSefv@34.221.2.149:27017/chris')
db = client.chris

# path of the folders for the images stored
#image_folder_path = "/opt/pythonApi/userImport/Images/" # set your folder file path
def user_import():
    image_folder_path = '/home/ubuntu/pairzy/images_600x600/'
    user_data_path = '/home/ubuntu/pairzy/user_data.csv'

    data_csv = pd.read_csv(user_data_path, encoding="ISO-8859-1")
    dataframe = pd.DataFrame(data=data_csv)
    details = dataframe.to_json(orient='records')
    data = json.loads(details)
    count = len(data)
    for i in data:
        user_data = {}
        current_date = datetime.datetime.now()

        current_next_year = datetime.datetime.now() + datetime.timedelta(days=365)
        current_next_year_timestamp = current_next_year.timestamp()

        current_timestamp = datetime.datetime.now().timestamp()
        current_isodate = datetime.datetime.now().replace(microsecond=0).isoformat()
        height = i['height']

        if not height:
            height = '''5' 10" (178 cm)'''

        if "<" in height.split("(")[0]:
            height_inch = (height.split("(")[0]).split("<")
            height_inch = height_inch[1]
        else:
            height_inch = height.split("(")[0]

        if "<" in height.split("(")[1]:
            height_cm = ((height.split("(")[1]).split("<"))[1]
            height_cm = height_cm.split("cm)")
            height_cm = height_cm[0]
        else:
            height_cm = (height.split("(")[1]).split("cm)")
            height_cm = height_cm[0]
        latitude = i['lat']
        longtitude = i['lng']
        first_name = i['first_name']
        email = i['email']
        gender = i['gender']
        contact_number = ''.join(random.choice(string.digits) for _ in range(10))
        contact_number = "+1"+contact_number
        countryCode= "+1"
        location_mongo =  {
            "longitude": longtitude,
            "latitude": latitude
        }
        location_search =  {
            "lon": longtitude,
            "lat": latitude
        }
        date_of_birth = (datetime.datetime.strptime(i['birthdate'], "%m/%d/%Y").timestamp())*1000
        registered_timestamp = int(current_timestamp*1000)

        image_name = image_folder_path + str(i['member_id']) + "_0.png"
        print(image_name)
        image = cloudinary.uploader.upload(image_folder_path+str(i['member_id'])+"_0.png")
        print(image)

        my_preferences = [
            {
                "pref_id": ObjectId("5a30fcb327322defa4a145f4"),
                "isDone": False
            },
            {
                "pref_id": ObjectId("5a30fda027322defa4a14638"),
                "isDone": False
            },
            {
                "pref_id": ObjectId("5a30fe6527322defa4a14673"),
                "isDone": False
            },
            {
                "pref_id": ObjectId("5a30ff7e27322defa4a146c4"),
                "isDone": False
            },
            {
                "pref_id": ObjectId("5a31002827322defa4a146f9"),
                "isDone": False
            },
            {
                "pref_id": ObjectId("5a31005027322defa4a14703"),
                "isDone": False
            }]
        my_preferences_elastic = [
            {
                "pref_id": "5a30fcb327322defa4a145f4",
                "isDone": False
            },
            {
                "pref_id": "5a30fda027322defa4a14638",
                "isDone": False
            },
            {
                "pref_id": "5a30fe6527322defa4a14673",
                "isDone": False
            },
            {
                "pref_id": "5a30ff7e27322defa4a146c4",
                "isDone": False
            },
            {
                "pref_id": "5a31002827322defa4a146f9",
                "isDone": False
            },
            {
                "pref_id": "5a31005027322defa4a14703",
                "isDone": False
            }]
        #================================for education prefrence====================================================
        eduction_data = db.userPrefrances.find({"label" : "Education"})

        for e in eduction_data:
            if i['education'] in e['options']:
                my_preferences.append({
                    "pref_id": ObjectId("5a30fdfa27322defa4a14653"),
                    "isDone": True,
                    "selectedValues": [i['education']]
                })
                my_preferences_elastic.append({
                    "pref_id": "5a30fdfa27322defa4a14653",
                    "isDone": True,
                    "selectedValues": [i['education']]
                })
            else:
                my_preferences.append({
                    "pref_id": ObjectId("5a30fdfa27322defa4a14653"),
                    "isDone": False
                })
                my_preferences_elastic.append({
                    "pref_id": "5a30fdfa27322defa4a14653",
                    "isDone": False
                })
        #================================for smoking prefrence====================================================
        smoking_data = db.userPrefrances.find({"label" : "Smoking"})

        for s in smoking_data:
            if i['smoke'] in e['options']:
                my_preferences.append({
                    "pref_id": ObjectId("5a31000227322defa4a146eb"),
                    "isDone": True,
                    "selectedValues": [i['smoke']]
                })
                my_preferences_elastic.append({
                    "pref_id": "5a31000227322defa4a146eb",
                    "isDone": True,
                    "selectedValues": [i['smoke']]
                })

            elif i['smoke'] == "Socially":
                my_preferences.append({
                    "pref_id": ObjectId("5a31000227322defa4a146eb"),
                    "isDone": True,
                    "selectedValues": ["Sometimes"]
                })
                my_preferences_elastic.append({
                    "pref_id": "5a31000227322defa4a146eb",
                    "isDone": True,
                    "selectedValues": ["Sometimes"]
                })

            elif i['smoke'] == "Never":
                my_preferences.append({
                    "pref_id": ObjectId("5a31000227322defa4a146eb"),
                    "isDone": True,
                    "selectedValues": ["No"]
                })
                my_preferences_elastic.append({
                    "pref_id": "5a31000227322defa4a146eb",
                    "isDone": True,
                    "selectedValues": ["No"]
                })

            else:
                my_preferences.append({
                    "pref_id": ObjectId("5a31000227322defa4a146eb"),
                    "isDone": False
                })
                my_preferences_elastic.append({
                    "pref_id": "5a31000227322defa4a146eb",
                    "isDone": False
                })

        #================================for drink prefrence====================================================
        drinking_data = db.userPrefrances.find({"label" : "Drinking"})

        for d in drinking_data:
            if i['drink'] in d['options']:
                my_preferences.append({
                    "pref_id": ObjectId("5a30ffb227322defa4a146d4"),
                    "isDone": True,
                    "selectedValues": [i['drink']]
                })
                my_preferences_elastic.append({
                    "pref_id": "5a30ffb227322defa4a146d4",
                    "isDone": True,
                    "selectedValues": [i['drink']]
                })
            elif i['drink'] == "Socially":
                my_preferences.append({
                    "pref_id": ObjectId("5a30ffb227322defa4a146d4"),
                    "isDone": True,
                    "selectedValues": ["Sometimes"]
                })
                my_preferences_elastic.append({
                    "pref_id": "5a30ffb227322defa4a146d4",
                    "isDone": True,
                    "selectedValues": ["Sometimes"]
                })
            else:
                my_preferences.append({
                    "pref_id": ObjectId("5a30ffb227322defa4a146d4"),
                    "isDone": False
                })
                my_preferences_elastic.append({
                    "pref_id": "5a30ffb227322defa4a146d4",
                    "isDone": False
                })

        #================================for family plan prefrence====================================================
        family_data = db.userPrefrances.find({"label" : "Family Plans"})
        for f in family_data:
            if i['want_children'] == "Yes":
                my_preferences.append({
                    "pref_id": ObjectId("5a30fd0e27322defa4a1460e"),
                    "isDone": True,
                    "selectedValues": ["Want kids"]
                })
                my_preferences_elastic.append({
                    "pref_id": "5a30fd0e27322defa4a1460e",
                    "isDone": True,
                    "selectedValues": ["Want kids"]
                })
            elif i['want_children'] == "No" or i['want_children'] == None:
                my_preferences.append({
                    "pref_id": ObjectId("5a30fd0e27322defa4a1460e"),
                    "isDone": True,
                    "selectedValues": ["Don't want kids"]
                })
                my_preferences_elastic.append({
                    "pref_id": "5a30fd0e27322defa4a1460e",
                    "isDone": True,
                    "selectedValues": ["Don't want kids"]
                })
            else:
                my_preferences.append({
                    "pref_id": ObjectId("5a30fd0e27322defa4a1460e"),
                    "isDone": False
                })
                my_preferences_elastic.append({
                    "pref_id": "5a30fd0e27322defa4a1460e",
                    "isDone": False
                })

        #================================for religion prefrence====================================================
        religious_data = db.userPrefrances.find({"label" : "Religious beliefs"})

        for r in religious_data:
            if i['religion'] in r['options']:
                my_preferences.append({
                    "pref_id": ObjectId("5a30ff2627322defa4a146a8"),
                    "isDone": True,
                    "selectedValues": [i['religion']]
                })
                my_preferences_elastic.append({
                    "pref_id": "5a30ff2627322defa4a146a8",
                    "isDone": True,
                    "selectedValues": [i['religion']]
                })
            else:
                my_preferences.append({
                    "pref_id": ObjectId("5a30ff2627322defa4a146a8"),
                    "isDone": False
                })
                my_preferences_elastic.append({
                    "pref_id": "5a30ff2627322defa4a146a8",
                    "isDone": False
                })

        #================================for occupation prefrence====================================================
        occupation_data = db.userPrefrances.find({"label" : "Job"})

        for o in occupation_data:
            if i['occupation'] in o['options']:
                my_preferences.append({
                    "pref_id": ObjectId("5a30fdd127322defa4a14649"),
                    "isDone": True,
                    "selectedValues": [i['occupation']]
                })
                my_preferences_elastic.append({
                    "pref_id": "5a30fdd127322defa4a14649",
                    "isDone": True,
                    "selectedValues": [i['occupation']]
                })
            else:
                my_preferences.append({
                    "pref_id": ObjectId("5a30fdd127322defa4a14649"),
                    "isDone": False
                })
                my_preferences_elastic.append({
                    "pref_id": "5a30fdd127322defa4a14649",
                    "isDone": False
                })

        #================================for race prefrence====================================================
        race_data = db.userPrefrances.find({"label" : "Ethnicity"})

        for race in race_data:
            if i['race'] in race['options']:
                my_preferences.append({
                    "pref_id": ObjectId("5a30fb2827322defa4a14586"),
                    "isDone": True,
                    "selectedValues": [i['race']]
                })
                my_preferences_elastic.append({
                    "pref_id": "5a30fb2827322defa4a14586",
                    "isDone": True,
                    "selectedValues": [i['race']]
                })
            else:
                my_preferences.append({
                    "pref_id": ObjectId("5a30fb2827322defa4a14586"),
                    "isDone": False
                })
                my_preferences_elastic.append({
                    "pref_id": "5a30fb2827322defa4a14586",
                    "isDone": False
                })

        #================================for height prefrence====================================================
        height_data = db.userPrefrances.find({"label" : "Height"})

        for height in height_data:
            if i['height'] in height['options']:
                my_preferences.append({
                    "pref_id": ObjectId("5a30fa6d27322defa4a14550"),
                    "isDone": True,
                    "selectedValues": [i['height']]
                })
                my_preferences_elastic.append({
                    "pref_id": "5a30fa6d27322defa4a14550",
                    "isDone": True,
                    "selectedValues": [i['height']]
                })
            else:
                my_preferences.append({
                    "pref_id": ObjectId("5a30fa6d27322defa4a14550"),
                    "isDone": False
                })
                my_preferences_elastic.append({
                    "pref_id": "5a30fa6d27322defa4a14550",
                    "isDone": False
                })
        online_status = 0
        search_preferences = [
            {
                "pref_id": ObjectId("55d486a5f6523c582bc51c53"),
                "selectedValue": [
                    18,
                    55
                ],
                "selectedUnit": "years"
            },
            {
                "pref_id": ObjectId("55d48f9bf6523cc552c51c52"),
                "selectedValue": [
                    0,
                    100
                ],
                "selectedUnit": "km"
            },
            {
                "pref_id": ObjectId("57231014e8408f292d8b4567"),
                "selectedValue": [
                    "Male"
                ]
            },
            {
                "pref_id": ObjectId("58bfb210849239b062ca9db4"),
                "selectedValue": [
                    121,
                    213
                ],
                "selectedUnit": "cm"
            }
        ]
        search_preferences_elastic =  [
            {
                "pref_id": "55d486a5f6523c582bc51c53",
                "selectedValue": [
                    "18",
                    "55"
                ],
                "selectedUnit": "years"
            },
            {
                "pref_id": "55d48f9bf6523cc552c51c52",
                "selectedValue": [
                    "0",
                    "100"
                ],
                "selectedUnit": "km"
            },
            {
                "pref_id": "57231014e8408f292d8b4567",
                "selectedValue": [
                    "Male"
                ]
            },
            {
                "pref_id": "58bfb210849239b062ca9db4",
                "selectedValue": [
                    "121",
                    "213"
                ],
                "selectedUnit": "cm"
            }
        ]

        mongo_id = ObjectId()
        user_data_mongo = {
            "_id": mongo_id,
            "firstName": first_name,
            "email": email,
            "contactNumber": contact_number,
            "countryCode": countryCode,
            "dob": date_of_birth,
            "gender": gender,
            "registeredTimestamp": float(registered_timestamp),
            "profilePic": image['url'],
            "profileVideo": "",
            "creationDate": current_isodate,
            "searchPreferences": search_preferences,
            "location": location_mongo,
            "myPreferences": my_preferences,
            "height": int(height_cm),
            "heightInFeet": height_inch,
            "onlineStatus": online_status,
            "likedBy": [],
            "myLikes": [],
            "myunlikes": [],
            "mySupperLike": [],
            "supperLikeBy": [],
            "disLikedUSers": [],
            "matchedWith": [],
            "lastUnlikedUser": [],
            "recentVisitors": [],
            "blockedBy": [],
            "myBlock": [],
            "supperLikeByHistory": [],
            "count": {
                "rewind": 0,
                "like": 0
            },
            "lastTimestamp": {
                "rewind": 0,
                "like": 0
            },
            "profileVideoWidth": "",
            "profileVideoHeight": "",
            "userType": "Normal",
            "address": {
                "city": i['city'],
                "country": i['country']
            },
            "profileStatus": 0,
            "subscription": [
                {
                    "planId": ObjectId("5b52d9901582421dee1bde50"),
                    "subscriptionId": "Free Plan",
                    "purchaseDate": current_timestamp,
                    "purchaseTime": current_timestamp,
                    "userPurchaseTime": current_timestamp,
                    "durationInMonths": 1,
                    "actualId": "nohaveidnow",
                    "actualIdForiOS": "nohaveidnow",
                    "actualIdForAndroid": "nohaveidnow",
                    "expiryTime": current_next_year_timestamp,
                    "likeCount": "100",
                    "rewindCount": "1",
                    "superLikeCount": "1",
                    "whoLikeMe": False,
                    "whoSuperLikeMe": False,
                    "recentVisitors": False,
                    "readreceipt": False,
                    "passport": False,
                    "noAdds": False,
                    "hideDistance": False,
                    "hideAge": False
                }
            ]
        }

        user_data_elastic = {
            "firstName": first_name,
            "email": email,
            "contactNumber": contact_number,
            "countryCode": countryCode,
            "dob": date_of_birth,
            "gender": gender,
            "registeredTimestamp": float(registered_timestamp),
            "profilePic": image['url'],
            "profileVideo": "",
            "creationDate": current_isodate,
            "searchPreferences": search_preferences_elastic,
            "location": location_search,
            "myPreferences": my_preferences_elastic,
            "height": int(height_cm),
            "heightInFeet": height_inch,
            "onlineStatus": online_status,
            "likedBy": [],
            "myLikes": [],
            "myunlikes": [],
            "mySupperLike": [],
            "supperLikeBy": [],
            "disLikedUSers": [],
            "matchedWith": [],
            "lastUnlikedUser": [],
            "recentVisitors": [],
            "blockedBy": [],
            "myBlock": [],
            "supperLikeByHistory": [],
            "count": {
                "rewind": 0,
                "like": 0
            },
            "lastTimestamp": {
                "rewind": 0,
                "like": 0
            },
            "profileVideoWidth": "",
            "profileVideoHeight": "",
            "userType": "Normal",
            "address": {
                "city": i['city'],
                "country": i['country']
            },
            "profileStatus": 0,
            "subscription": [
                {
                    "planId": "5b52d9901582421dee1bde50",
                    "subscriptionId": "Free Plan",
                    "purchaseDate": current_timestamp,
                    "purchaseTime": current_timestamp,
                    "userPurchaseTime": current_timestamp,
                    "durationInMonths": 1,
                    "actualId": "nohaveidnow",
                    "actualIdForiOS": "nohaveidnow",
                    "actualIdForAndroid": "nohaveidnow",
                    "expiryTime": current_next_year_timestamp,
                    "likeCount": "100",
                    "rewindCount": "1",
                    "superLikeCount": "1",
                    "whoLikeMe": False,
                    "whoSuperLikeMe": False,
                    "recentVisitors": False,
                    "readreceipt": False,
                    "passport": False,
                    "noAdds": False,
                    "hideDistance": False,
                    "hideAge": False
                }
            ]
        }
        es_res = es.index(index="devchrish", doc_type="userList", id=str(mongo_id), body=user_data_elastic)
        print(es_res)
        mongo_res = db.userList.insert(user_data_mongo)
        user_coin_data = {
            "_id" : ObjectId(mongo_id),
            "coins" : {
                "Coin" : 0
            }
        }
        db.coinWallet.insert(user_coin_data)
        count = count - 1
        print(count)
    response = {"message": "data uploaded successfully..!!!"}
    return response

if __name__ == "__main__":
    print("Your Data Uploading....!!!!")
    data = user_import()

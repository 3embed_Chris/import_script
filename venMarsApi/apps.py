from django.apps import AppConfig


class VenmarsapiConfig(AppConfig):
    name = 'venMarsApi'
